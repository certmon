# This file is part of certmon
# Copyright (C) 2019 Sergey Poznyakoff
#
# Certmon is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Certmon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with certmon.  If not, see <http://www.gnu.org/licenses/>.

PACKAGE = certmon
VERSION = 1.0
SOURCES = certmon.go
MANPAGE = certmon.8

PREFIX  = /usr/local
BINDIR  = $(PREFIX)/bin
MANDIR  = $(PREFIX)/share/man
MAN8DIR = $(MANDIR)/man8

all:
	@go build -o $(PACKAGE) $(SOURCES)

clean:
	@go clean

install: install-man
	@GOBIN=$(DESTDIR)$$(if test -n "$(NAGIOS_CONF)" && \
                    test -f "$(NAGIOS_CONF)/resource.cfg"; then \
                    sed -n '/^\$$USER1\$$=/s///p' $$NAGIOS_CONF; \
	         else \
	            echo $(BINDIR); \
	         fi) \
	go install .

install-man:
	@if ! test -d $(DESTDIR)$(MAN8DIR); then \
	    mkdir -p $(DESTDIR)$(MAN8DIR); \
	fi;
	@cp $(MANPAGE) $(DESTDIR)$(MAN8DIR);

DISTDIR   = $(PACKAGE)-$(VERSION)
DISTFILES = $(SOURCES) $(MANPAGE) README NEWS COPYING Makefile 

distdir:
	@test -d $(DISTDIR) || mkdir $(DISTDIR)
	@cp $(DISTFILES) $(DISTDIR)

dist: distdir
	@tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	@rm -rf $(DISTDIR)

distcheck: dist
	@tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS); then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
	fi

