certmon README.
See the end of file for copying conditions.

* Overview

This is a Nagios[1] plugin for monitoring expiration times of remote SSL
certificates via HTTPS.  It can also be used as a stand-alone program.

The following Nagios configuration snippet defines the
a command for checking remote certificate expiry and a service that
uses it:

  define command {
    command_name check_cert
    command_line $USER1$/certmon -H $HOSTADDRESS$ -w 48h -c 24h $ARG1$
  }

  define service {
    host_name           example.org
    service_description Certificate for example.org
    check_command       check_cert!example.org www.example.org
  }

The check will emit warning if the certificate expires within 48 hours
and a critical condition if it expires within 24 hours from the moment
of the check.

* Installation

You will need the golang compiler version 1.11.4 or newer.  To build,
run make.  To build and install the program, run:

  make install [VAR=VALUE ...]

By default, the binary will be installed to /usr/local/bin and the
manpage to /usr/local/share/man/man8.  The following variables can be
set in the command line in order to change these defaults:

- PREFIX

General installation prefix.  It defaults to /usr/local.  The defaults
for the remaining variables are defined using this prefix as follows:

  BINDIR=$(PREFIX)/bin
  MANDIR=$(PREFIX)/share/man
  MAN8DIR=$(MANDIR)/man8

Thus, running "make install PREFIX=/usr" is a quick way to install
everything under the /usr directory.

- BINDIR

Directory to install the binary to.

- MANDIR

Root directory for installing manpages.

- MAN8DIR

Directory to install the certmon.8 manpage to.

- NAGIOS_CONF

Pathname of the Nagios configuration directory.  If it is supplied,
the file "$(NAGIOS_CONF)/resource.cfg" will be scanned for the
definition of the user-defined variable $USER1$, and its value will be
assigned to BINDIR.  This allows for quick installation of certmon to
the place where the rest of Nagios plugins reside, e.g.:

  make NAGIOS_CONF=/etc/nagios


- DESTDIR

Destination prefix.  It is added to the beginning of each of the
variables discussed above.  This is useful for packaging.  Defaults to
empty string.

* Documentation

Manpage certmon.8 is included. After installing the package, run
man certmon.
    
* Bug reporting.		

Send bug reports to <gray+certmon@gnu.org.ua>. 

* References

[1] nagios - https://www.nagios.org


* Copyright information:

Copyright (C) 2019 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:


